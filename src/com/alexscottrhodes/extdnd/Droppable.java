package com.alexscottrhodes.extdnd;

import java.util.Map;

import javax.faces.component.FacesComponent;
import javax.faces.component.UIData;
import javax.faces.component.UINamingContainer;
import javax.faces.context.FacesContext;
import javax.faces.event.AjaxBehaviorEvent;
import javax.faces.event.FacesEvent;

import org.primefaces.event.DragDropEvent;
import org.primefaces.util.Constants;

@FacesComponent(tagName = "extDroppable", createTag = true, namespace = "http://alexscottrhodes.com/facelets/pfe", value = "com.alexscottrhodes.extDroppable")
public class Droppable extends org.primefaces.component.dnd.Droppable{

	  @Override
	    public void queueEvent(FacesEvent event) {
	        FacesContext context = getFacesContext();
	        if(isRequestSource(context)) {
	            Map<String,String> params = context.getExternalContext().getRequestParameterMap();
	            String eventName = params.get(Constants.RequestParams.PARTIAL_BEHAVIOR_EVENT_PARAM);
	            String clientId = getClientId(context);

	            AjaxBehaviorEvent behaviorEvent = (AjaxBehaviorEvent) event;

	            if(eventName.equals("drop")) {
	                String dragId = params.get(clientId + "_dragId");
	                String dropId = params.get(clientId + "_dropId");
	                DragDropEvent dndEvent = null;
	                String datasourceId = getDatasource();
	                if(datasourceId != null) {
	                	String[] datasourceIdList;
	  	                if(datasourceId.contains(",")){
	  	                	datasourceIdList = datasourceId.split(",");
	  	                }else{
	  	                	datasourceIdList = new String[1];
	  	                	datasourceIdList[0] = datasourceId;
	  	                }
	  	                
	  	                Object data = null;
	  	                for(String id : datasourceIdList){
	  	                	if(dragId.contains(id)){
			                    UIData datasource = findDatasource(context, this, id);
			                    String[] idTokens = dragId.split(String.valueOf(UINamingContainer.getSeparatorChar(context)));
			                    int rowIndex = Integer.parseInt(idTokens[idTokens.length - 2]);
			                    datasource.setRowIndex(rowIndex);
				                    try{
				                    	data = datasource.getRowData();
				                    	datasource.setRowIndex(-1);
				                    	break;
				                    }catch(Exception e){
				                    	continue;
				                    }
	  	                	}
	  	                }
	  	                if(data != null){
	  	                	dndEvent = new DragDropEvent(this, behaviorEvent.getBehavior(), dragId, dropId, data);	  	                	
	  	                	//return (java.lang.String) getStateHelper().eval(PropertyKeys.datasource, null);
	  	                }else{
	  	                	throw new RuntimeException("Invalid datasource or no row data available for given data sources.");
	  	                }
	  	            }else{
		                dndEvent = new DragDropEvent(this, behaviorEvent.getBehavior(), dragId, dropId);
	                }
	                
	                
	                this.getParent().queueEvent(dndEvent);
	            }
	            
	        }
	        else {
	          this.getParent().queueEvent(event);
	        }
	    }
	 
	    private boolean isRequestSource(FacesContext context) {
	        return this.getClientId(context).equals(context.getExternalContext().getRequestParameterMap().get(Constants.RequestParams.PARTIAL_SOURCE_PARAM));
	    }
	    
}
