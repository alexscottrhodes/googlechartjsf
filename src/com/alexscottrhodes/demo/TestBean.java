package com.alexscottrhodes.demo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import org.primefaces.component.dnd.Droppable;
import org.primefaces.context.RequestContext;
import org.primefaces.event.DragDropEvent;
import org.primefaces.event.ReorderEvent;

@ManagedBean
@ViewScoped
public class TestBean {

	private HashMap<String,ArrayList<String>> catList = new HashMap<String,ArrayList<String>>();
	private ArrayList<String> t1,t2,t3;
	private String dataSourceIdString = "";
	public TestBean(){
		t1 = new ArrayList<String>();
		t2 = new ArrayList<String>();
		t3 = new ArrayList<String>();
		t1.add("field 1");
		t1.add("field 2");
		t1.add("field 3");
		catList.put("list 1",t1);
		catList.put("list 2", t2);
		catList.put("list 3",t3);
		generateDsId();
	}
	
	
	public String getId(int test){
		System.out.println(test);
		return "lim";
	}

	
	
	public ArrayList<String> getLists(){
		return new ArrayList<String>(catList.keySet());
	}
	
	public ArrayList<String> getCardsByList(int listPos){
		return catList.get(new ArrayList<String>(catList.keySet()).get(listPos));
	}
	
	public void wasDrop(DragDropEvent ddEvent) {
		
		//Parsing strings and table identities etc
		String dragId = ddEvent.getDragId();
		String dropId = ddEvent.getDropId();
		String data = (String) ddEvent.getData();
		String sourceTable = dragId.substring(dragId.indexOf("agTable"));
		sourceTable = sourceTable.substring(0, sourceTable.indexOf(":"));
		int sourceIndex = Integer.valueOf(sourceTable.replace("agTable",""));
		int destIndex = Integer.valueOf(dropId.replace("agile:drop", ""));
	
		
		//Remove from origin list
		int i = 0;
		for(Map.Entry<String, ArrayList<String>> entry : catList.entrySet()){
			if(i==sourceIndex){
				Iterator<String> lit = entry.getValue().iterator();
					while(lit.hasNext()){
						if(lit.next().equals(data)){
							lit.remove();
							break;
						}
					}
					break;
				}
			i++;
			}
		
		//Add to destination list
		i = 0;
		for(Map.Entry<String, ArrayList<String>> entry : catList.entrySet()){
			if(i==destIndex){
				entry.getValue().add(0,data);
				break;
			}
			i++;
		}
		
		RequestContext.getCurrentInstance().update("agile");
    }
	
	
	public void onReorder(ReorderEvent event){
		String temp =(String) event.getSource();
		System.out.println(temp);
	}
	
	
	public void generateDsId(){
		String ds = "";
		for(int i = 0; i < catList.size(); i++){
			ds+="agTable" + i +",";
		}
		ds = ds.substring(0,ds.lastIndexOf(","));
		dataSourceIdString = ds;
	}
	
	public String getDataSourceString(){
		return dataSourceIdString;
	}


	public ArrayList<String> getT1() {
		return t1;
	}


	public void setT1(ArrayList<String> t1) {
		this.t1 = t1;
	}


	public ArrayList<String> getT2() {
		return t2;
	}


	public void setT2(ArrayList<String> t2) {
		this.t2 = t2;
	}


	public ArrayList<String> getT3() {
		return t3;
	}


	public void setT3(ArrayList<String> t3) {
		this.t3 = t3;
	}
	
}
